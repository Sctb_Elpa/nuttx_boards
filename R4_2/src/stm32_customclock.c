/****************************************************************************
 * configs/R4_2/src/stm32_customclock.c
 *
 *   Copyright (C) 2009, 2011-2012, 2015 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *   		 SweetTressure SweetTresure@2ch.hk
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


/****************************************************************************
 * This board have external clock source on OSC_IN pin
 ****************************************************************************/

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Allow up to 100 milliseconds for the high speed clock to become ready.
 * that is a very long delay, but if the clock does not become ready we are
 * hosed anyway.  Normally this is very fast, but I have seen at least one
 * board that required this long, long timeout for the HSE to be ready.
 */

#include <arch/board/board.h>

#define HSERDY_TIMEOUT (100 * CONFIG_BOARD_LOOPSPERMSEC)

void stm32_board_clockconfig(void)
{
	uint32_t regval;

	/* If the PLL is using the HSE, or the HSE is the system clock */

#if (STM32_CFGR_PLLSRC == RCC_CFGR_PLLSRC) || (STM32_SYSCLK_SW == RCC_CFGR_SW_HSE)
		/* Enable External High-Speed Clock (HSE) */
		regval  = getreg32(STM32_RCC_CR);
		regval &= ~RCC_CR_HSEON;           	/* Disable HSE */
		putreg32(regval, STM32_RCC_CR);

#	ifdef CONFIG_R4_2_EXTERNAL_CLOCK
		// enable external clock bypass
		regval  = getreg32(STM32_RCC_CR);
		regval |= RCC_CR_HSEBYP;         	/* Enable HSE clock bypass */
		putreg32(regval, STM32_RCC_CR);

		// enable external HSE
		regval  = getreg32(STM32_RCC_CR);
		regval |= RCC_CR_HSEON;           	/* Enable HSE */
		putreg32(regval, STM32_RCC_CR);
#	endif

	/* If this is a value-line part and we are using the HSE as the PLL */

#	if defined(CONFIG_STM32_VALUELINE) && (STM32_CFGR_PLLSRC == RCC_CFGR_PLLSRC)

# 		if (STM32_CFGR_PLLXTPRE >> 17) != (STM32_CFGR2_PREDIV1 & 1)
#  			error STM32_CFGR_PLLXTPRE must match the LSB of STM32_CFGR2_PREDIV1
# 	endif

	/* Set the HSE prescaler */

	regval = STM32_CFGR2_PREDIV1;
	putreg32(regval, STM32_RCC_CFGR2);

# 	endif
#endif

	/* Value-line devices don't implement flash prefetch/waitstates */

#ifndef CONFIG_STM32_VALUELINE

	/* Enable FLASH prefetch buffer and 2 wait states */

	regval  = getreg32(STM32_FLASH_ACR);
	regval &= ~FLASH_ACR_LATENCY_MASK;
	regval |= (FLASH_ACR_LATENCY_2|FLASH_ACR_PRTFBE);
	putreg32(regval, STM32_FLASH_ACR);

#endif

	/* Set the HCLK source/divider */

	regval = getreg32(STM32_RCC_CFGR);
	regval &= ~RCC_CFGR_HPRE_MASK;
	regval |= STM32_RCC_CFGR_HPRE;
	putreg32(regval, STM32_RCC_CFGR);

	/* Set the PCLK2 divider */

	regval = getreg32(STM32_RCC_CFGR);
	regval &= ~RCC_CFGR_PPRE2_MASK;
	regval |= STM32_RCC_CFGR_PPRE2;
	putreg32(regval, STM32_RCC_CFGR);

	/* Set the PCLK1 divider */

	regval = getreg32(STM32_RCC_CFGR);
	regval &= ~RCC_CFGR_PPRE1_MASK;
	regval |= STM32_RCC_CFGR_PPRE1;
	putreg32(regval, STM32_RCC_CFGR);

	/* If we are using the PLL, configure and start it */

#if STM32_SYSCLK_SW == RCC_CFGR_SW_PLL

	/* Set the PLL divider and multiplier */

	regval = getreg32(STM32_RCC_CFGR);
	regval &= ~(RCC_CFGR_PLLSRC|RCC_CFGR_PLLXTPRE|RCC_CFGR_PLLMUL_MASK);
	regval |= (STM32_CFGR_PLLSRC|STM32_CFGR_PLLXTPRE|STM32_CFGR_PLLMUL);
	putreg32(regval, STM32_RCC_CFGR);

	/* Enable the PLL */

	regval = getreg32(STM32_RCC_CR);
	regval |= RCC_CR_PLLON;
	putreg32(regval, STM32_RCC_CR);

	/* Wait until the PLL is ready */

	while ((getreg32(STM32_RCC_CR) & RCC_CR_PLLRDY) == 0);

#endif

	/* Select the system clock source (probably the PLL) */

	regval  = getreg32(STM32_RCC_CFGR);
	regval &= ~RCC_CFGR_SW_MASK;
	regval |= STM32_SYSCLK_SW;
	putreg32(regval, STM32_RCC_CFGR);

	/* Wait until the selected source is used as the system clock source */

	while ((getreg32(STM32_RCC_CFGR) & RCC_CFGR_SWS_MASK) != STM32_SYSCLK_SWS);

#if defined(CONFIG_STM32_IWDG) || defined(CONFIG_RTC_LSICLOCK)
	/* Low speed internal clock source LSI */

	stm32_rcc_enablelsi();
#endif

#if defined(CONFIG_RTC_LSECLOCK)
	/* Low speed external clock source LSE */
	//  костыль, сразу настроим вход клока для часов

	putreg32(RCC_AHBENR_FLITFEN | RCC_AHBENR_SRAMEN, STM32_RCC_AHBENR);
	putreg32(RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN, STM32_RCC_APB1ENR);
	// enable backup modify
	bool wasenabled = stm32_pwr_enablebkp(true);
	modifyreg16(STM32_RCC_BDCR, 0, RCC_BDCR_RTCSEL_LSE);
	modifyreg16(STM32_RCC_BDCR, 0, RCC_BDCR_RTCEN);
	// disable backup modify
	/*if (!wasenabled) {
		stm32_pwr_enablebkp(false);
	}*/
#endif
}
