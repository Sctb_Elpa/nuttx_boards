/*
 * stm32_timers.c
 *
 *  Created on: 01 июня 2015 г.
 *      Author: tolyan
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>
#include <stdio.h>

#include <arch/board/board.h>

#include "stm32_dma.h"
#include "stm32_tim.h"
#include "stm32_dbgmcu.h"

#include "r4_2.h"

#include "../include/stm32_timers.h"

#include "stm32_tim.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/*
 * TIMERS CONFIGURATION:
 *
 * TIM2 - counts Pressure Frequency
 * INPUT - TIM2_CH1_ETR
 *
 * TIM4 - counts Temperature Frequency
 * INPUT - TIM4_CH2
 *
 * TIM1 - counts Reference Frequency = F(APB2)
 */

#define FP_TIMER_NUM 					2
#define FT_TIMER_NUM 					4
#define FMASTER_TIMER_NUM 				1

#define START_RELOAD_VALUE				100000
#define TIMER_FIRST_TIME_VALUE			500

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct stm32_tim_priv_s
{
	struct stm32_tim_ops_s *ops;
	stm32_tim_mode_t        mode;
	uint32_t                base;   /* TIMn base address */
};

struct sTIM2_DMA_DATA
{
	DMA_HANDLE dmaHandle[2];
	union un16bitCounter masterStartValue;
	union un16bitCounter masterStopValue;
};

struct sTIM_UP_DMA_DATA
{
	DMA_HANDLE dmaHandle;
	union un16bitCounter masterValues[2];
	uint16_t tmp_val;
	int index;
};

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const char errMsg[] = "ERROR: stm32_tim_init(%d) failed\n";
static struct stm32_tim_dev_s *Fp_counter, *Ft_counter, *Fmaster_counter;

static struct mesureResult Fp_Mesure_result, Ft_Mesure_result;
static uint16_t master_reload_count = 0;

//static struct sTIM2_DMA_DATA Timer2_Dma;
static struct sTIM_UP_DMA_DATA Timer4_Dma;
static struct sTIM_UP_DMA_DATA Timer2_Dma;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void stm32_tim_set_reload(
		FAR struct stm32_tim_dev_s *dev,
		uint16_t newreload);
static uint16_t stm32_tim_get_reload(FAR struct stm32_tim_dev_s *dev);
static void stm32_tim_set_prescaler(
		FAR struct stm32_tim_dev_s *dev,
		uint16_t newprescaler);
static uint16_t stm32_tim_get_prescaler(FAR struct stm32_tim_dev_s *dev);
static void stm32_tim_reload_counter(FAR struct stm32_tim_dev_s *dev);
static void stm32_tim_disable(FAR struct stm32_tim_dev_s *dev);
static void stm32_tim_enable(FAR struct stm32_tim_dev_s *dev);


/* Get a 16-bit register value by offset */

static inline uint16_t stm32_getreg16(FAR struct stm32_tim_dev_s *dev,
		uint8_t offset)
{
	return getreg16(((struct stm32_tim_priv_s *)dev)->base + offset);
}

/* Put a 16-bit register value by offset */

static inline void stm32_putreg16(FAR struct stm32_tim_dev_s *dev, uint8_t offset,
		uint16_t value)
{
	putreg16(value, ((struct stm32_tim_priv_s *)dev)->base + offset);
}

// -- ISR ---

static int Fmaster_ISR(int irq, void *context)
{
	master_reload_count++;
	STM32_TIM_ACKINT(Fmaster_counter, irq);
	return OK;
}

static void Fp_dma_callback(DMA_HANDLE handle, uint8_t status, void *arg)
{
	uint16_t master_ovf = master_reload_count;
	int index = Timer2_Dma.index;

	// relocate values
	Timer2_Dma.masterValues[index].s.Register = Timer2_Dma.tmp_val;
	Timer2_Dma.masterValues[index].s.Extantion = master_ovf;

	if (index)
	{
		///STOP
		Timer2_Dma.index = 0;

		Fp_Mesure_result.value = Timer2_Dma.masterValues[1].value -
				Timer2_Dma.masterValues[0].value;

		Fp_Mesure_result.period = stm32_tim_get_reload(Fp_counter) *
				stm32_tim_get_prescaler(Fp_counter);
		Fp_Mesure_result.ready = Fp_Mesure_result.Detector_ok;

		// reconfigure if need
		if (Fp_Mesure_result.period != Fp_Mesure_result.newperiod)
		{
			if (Fp_Mesure_result.newperiod < 2)
				Fp_Mesure_result.newperiod = 2;

			uint16_t prescaler, reload;
			const uint32_t base = ((struct stm32_tim_priv_s *)Fp_counter)->base;

			stm32_tim_disable(Fp_counter);

			prescaler = Fp_Mesure_result.newperiod / 65535 + 1;
			reload = Fp_Mesure_result.newperiod / prescaler;

			stm32_tim_set_prescaler(Fp_counter, prescaler);
			stm32_tim_set_reload(Fp_counter, reload);

			stm32_tim_enable(Fp_counter);
		}
	}
	else
	{
		///START
		Timer2_Dma.index = 1;
	}
}


static void Ft_dma_callback(DMA_HANDLE handle, uint8_t status, void *arg)
{
	uint16_t master_ovf = master_reload_count;
	int index = Timer4_Dma.index;

	// relocate values
	Timer4_Dma.masterValues[index].s.Register = Timer4_Dma.tmp_val;
	Timer4_Dma.masterValues[index].s.Extantion = master_ovf;

	if (index)
	{
		///STOP
		Timer4_Dma.index = 0;

		Ft_Mesure_result.value = Timer4_Dma.masterValues[1].value -
				Timer4_Dma.masterValues[0].value;

		Ft_Mesure_result.period = stm32_tim_get_reload(Ft_counter) *
				stm32_tim_get_prescaler(Ft_counter);
		Ft_Mesure_result.ready = Ft_Mesure_result.Detector_ok;

		// reconfigure if need
		if (Ft_Mesure_result.period != Ft_Mesure_result.newperiod)
		{
			if (Ft_Mesure_result.newperiod < 2)
				Ft_Mesure_result.newperiod = 2;

			uint16_t prescaler, reload;
			const uint32_t base = ((struct stm32_tim_priv_s *)Ft_counter)->base;

			stm32_tim_disable(Ft_counter);

			prescaler = Ft_Mesure_result.newperiod / 65535 + 1;
			reload = Ft_Mesure_result.newperiod / prescaler;

			stm32_tim_set_prescaler(Ft_counter, prescaler);
			stm32_tim_set_reload(Ft_counter, reload);

			stm32_tim_enable(Ft_counter);
		}
	}
	else
	{
		///START
		Timer4_Dma.index = 1;
	}
}

//------------------------


int enable_master_clock_timer(bool enable)
{
	if (enable && !Fmaster_counter)
	{
		Fmaster_counter = stm32_tim_init(FMASTER_TIMER_NUM);
		if (!Fmaster_counter)
		{
			fprintf(stderr, errMsg, FMASTER_TIMER_NUM);
			return -ENODEV;
		}

		modifyreg32(STM32_DBGMCU_CR, 0, DBGMCU_CR_TIM1STOP); // stop timer on cpu halted

		STM32_TIM_SETISR(Fmaster_counter, Fmaster_ISR, 0);
		STM32_TIM_ENABLEINT(Fmaster_counter, 0);

		STM32_TIM_SETCLOCK(Fmaster_counter, R4_2_MASTER_FREQUENCY);

		return OK;
	}
	else if (Fmaster_counter && !enable)
	{
		STM32_TIM_DISABLEINT(Fmaster_counter, 0);
		stm32_tim_deinit(Fmaster_counter);
		Fmaster_counter = NULL;
	}
	return OK;
}

static void stm32_tim_set_reload(
		FAR struct stm32_tim_dev_s *dev,
		uint16_t newreload)
{
	stm32_putreg16(dev, STM32_GTIM_ARR_OFFSET, newreload);
}

static uint16_t stm32_tim_get_reload(FAR struct stm32_tim_dev_s *dev)
{
	return stm32_getreg16(dev, STM32_GTIM_ARR_OFFSET);
}

static void stm32_tim_set_prescaler(
		FAR struct stm32_tim_dev_s *dev,
		uint16_t newprescaler)
{
	stm32_putreg16(dev, STM32_GTIM_PSC_OFFSET, newprescaler - 1);
}

static uint16_t stm32_tim_get_prescaler(FAR struct stm32_tim_dev_s *dev)
{
	return stm32_getreg16(dev, STM32_GTIM_PSC_OFFSET) + 1;
}

static void stm32_tim_reload_counter(FAR struct stm32_tim_dev_s *dev)
{
	uint16_t val = stm32_getreg16(dev, STM32_BTIM_EGR_OFFSET);
	val |= ATIM_EGR_UG;
	stm32_putreg16(dev, STM32_BTIM_EGR_OFFSET, val);
}

static void stm32_tim_enable(FAR struct stm32_tim_dev_s *dev)
{
	uint16_t val = stm32_getreg16(dev, STM32_BTIM_CR1_OFFSET);
	val |= ATIM_CR1_CEN;
	stm32_tim_reload_counter(dev);
	stm32_putreg16(dev, STM32_BTIM_CR1_OFFSET, val);
}

static void stm32_tim_disable(FAR struct stm32_tim_dev_s *dev)
{
	uint16_t val = stm32_getreg16(dev, STM32_BTIM_CR1_OFFSET);
	val &= ~ATIM_CR1_CEN;
	stm32_putreg16(dev, STM32_BTIM_CR1_OFFSET, val);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int enable_chanel(enum enMesureType chanel, bool enable)
{
	uint32_t base;

	switch (chanel)
	{
	case MT_PRESSURE:
		if (enable)
		{
			stm32_configgpio(GPIO_F_P); 	/* F_P */

			if (enable_master_clock_timer(true) != OK)
				return -ENODEV;

			// TIM2
			Fp_counter = stm32_tim_init(FP_TIMER_NUM);
			if (!Fp_counter)
			{
				fprintf(stderr, errMsg, FP_TIMER_NUM);
				return -ENODEV;
			}

			Fp_Mesure_result.Detector_ok = FAILURE_DETECTOR_OK;

			modifyreg32(STM32_DBGMCU_CR, 0, DBGMCU_CR_TIM2STOP); // stop timer on cpu halted

			base = ((struct stm32_tim_priv_s *)Fp_counter)->base;

			putreg16(GTIM_SMCR_ETP | //  Falling adge
					GTIM_SMCR_ECE | // External clock mode 2
					GTIM_SMCR_PSCOFF | // no prescaler
					ATIM_SMCR_NOFILT | // no filter
					GTIM_SMCR_ETRF, // External Trigger input
					base + STM32_GTIM_SMCR_OFFSET);

			modifyreg16(base + STM32_GTIM_CR1_OFFSET,
					0, GTIM_CR1_DIR); // downcounter

			///DMA
			Timer2_Dma.dmaHandle = stm32_dmachannel(DMACHAN_TIM2_UP);
			stm32_dmasetup(Timer2_Dma.dmaHandle,			// канал
					STM32_TIM1_CNT, 						// адрес в перефирийном устройстве
					&Timer2_Dma.tmp_val, 					// адрес в памяти
					1, // 1 раз
					DMA_CCR_PRIVERYHI | // Channel priority level - very high
					DMA_CCR_MSIZE_16BITS | // количество бит в памяти на 1 посылку - по 16
					DMA_CCR_PSIZE_16BITS | // аналогично для перефирии
					DMA_CCR_CIRC | // circular mode
					//DMA_CCR_MINC | // при записи в память не инкриментировать адрес
					//!DMA_CCR_PINC | // при чтении из перефирии не инкриментировать адрес
					//!DMA_CCR_DIR   // (0) Read from peripheral write to memory
					0
			);

			stm32_dmastart(Timer2_Dma.dmaHandle, Fp_dma_callback, NULL, false);
			Timer2_Dma.index = 0;
			///

			stm32_tim_set_reload(Fp_counter, TIMER_FIRST_TIME_VALUE);

			modifyreg16(base + STM32_GTIM_DIER_OFFSET,
					0,
					GTIM_DIER_UDE); // enable dma request

			stm32_tim_enable(Fp_counter);
		}
		else if (Fp_counter)
		{
			stm32_dmastop(Timer2_Dma.dmaHandle);
			stm32_dmafree(Timer2_Dma.dmaHandle);

			stm32_tim_deinit(Fp_counter);
			Fp_counter = NULL;

			if (!Ft_counter)
				enable_master_clock_timer(false);
		}
		break;
	case MT_TEMPERATURE:
		if (enable)
		{
			stm32_configgpio(GPIO_F_T); 	/* F_T */

			if (enable_master_clock_timer(true) != OK)
				return -ENODEV;

			// TIM4
			Ft_counter = stm32_tim_init(FT_TIMER_NUM);
			if (!Ft_counter)
			{
				fprintf(stderr, errMsg, FT_TIMER_NUM);
				return -ENODEV;
			}

			Ft_Mesure_result.Detector_ok = FAILURE_DETECTOR_OK;

			modifyreg32(STM32_DBGMCU_CR, 0, DBGMCU_CR_TIM4STOP); // stop timer on cpu halted

			base = ((struct stm32_tim_priv_s *)Ft_counter)->base;

			putreg16(GTIM_CCMR_ICF_NOFILT << GTIM_CCMR1_IC2F_SHIFT, // no input filter
					base + STM32_GTIM_CCR1_OFFSET);

			putreg16(GTIM_CCER_CC2P, // falling edge
					base + STM32_GTIM_CCER_OFFSET);

			putreg16(GTIM_SMCR_TI2FP2 | // input is Filtered Timer Input 2
					ATIM_SMCR_EXTCLK1, 	// External Clock Mode 1
					base + STM32_GTIM_SMCR_OFFSET);

			modifyreg16(base + STM32_GTIM_CR1_OFFSET,
					0, GTIM_CR1_DIR); // downcounter


			///DMA
			Timer4_Dma.dmaHandle = stm32_dmachannel(DMACHAN_TIM4_UP);
			stm32_dmasetup(Timer4_Dma.dmaHandle,			// канал
					STM32_TIM1_CNT, 						// адрес в перефирийном устройстве
					&Timer4_Dma.tmp_val, 					// адрес в памяти
					1, // 1 раз
					DMA_CCR_PRIVERYHI | // Channel priority level - very high
					DMA_CCR_MSIZE_16BITS | // количество бит в памяти на 1 посылку - по 16
					DMA_CCR_PSIZE_16BITS | // аналогично для перефирии
					DMA_CCR_CIRC | // circular mode
					//DMA_CCR_MINC | // при записи в память не инкриментировать адрес
					//!DMA_CCR_PINC | // при чтении из перефирии не инкриментировать адрес
					//!DMA_CCR_DIR   // (0) Read from peripheral write to memory
					0
			);

			stm32_dmastart(Timer4_Dma.dmaHandle, Ft_dma_callback, NULL, false);
			Timer4_Dma.index = 0;
			///

			stm32_tim_set_reload(Ft_counter, TIMER_FIRST_TIME_VALUE);

			modifyreg16(base + STM32_GTIM_DIER_OFFSET,
					0,
					GTIM_DIER_UDE); // enable dma request

			stm32_tim_enable(Ft_counter);
		}
		else if (Ft_counter)
		{
			stm32_dmastop(Timer4_Dma.dmaHandle);
			stm32_dmafree(Timer4_Dma.dmaHandle);

			stm32_tim_deinit(Ft_counter);
			Ft_counter = NULL;

			if (!Fp_counter)
				enable_master_clock_timer(false);
		}
		break;
	default:
		return -EINVAL;
	}
	return OK;
}

struct mesureResult* getResultPointer(enum enMesureType chanel)
{
	switch(chanel)
	{
	case MT_PRESSURE: return &Fp_Mesure_result;
	case MT_TEMPERATURE: return &Ft_Mesure_result;
	default: return NULL;
	}
}

