/*
 * r4_2_bt.c
 *
 *  Created on: 23 июня 2015 г.
 *      Author: tolyan
 */

#include <nuttx/config.h>

#include <stdint.h>

#include "chip.h"
#include "stm32.h"
#include "r4_2.h"

void r4_2_bt_init(void)
{
	stm32_configgpio(GPIO_BT_LINK);
	stm32_configgpio(GPIO_BT_RESET);
}

void r4_2_bt_reset(void)
{
	int i;

	// double reset, its good!
	stm32_gpiowrite(GPIO_BT_RESET, 0);
	for(i = 1000000;i > 0; --i)
		asm volatile ("nop");
	stm32_gpiowrite(GPIO_BT_RESET, 1);
	for(i = 1000000;i > 0; --i)
			asm volatile ("nop");
	stm32_gpiowrite(GPIO_BT_RESET, 0);
	for(i = 1000000;i > 0; --i)
		asm volatile ("nop");
	stm32_gpiowrite(GPIO_BT_RESET, 1);
}
