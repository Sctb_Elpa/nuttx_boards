/************************************************************************************
 * configs/r4-2/src/spark.h
 *
 *   Copyright (C) 2013 Gregory Nutt. All rights reserved.
 *   Author: Laurent Latil <laurent@latil.nom.fr>
 *           Librae <librae8226@gmail.com>
 *           David_s5 <david_s5@nscdg.com>
 *           Shilo_XyZ_ <shilo.xyz@qip.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __CONFIGS_SPARK_SRC_SPARK_H
#define __CONFIGS_SPARK_SRC_SPARK_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <stdint.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/*
 * Board GPIO Usage:
 *
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *  PA[00] WKUP/USART2_CTS/ADC12_IN0/TIM2_CH1_ETR            10       F_P
 *  PA[01] USART2_RTS/ADC12_IN1/TIM2_CH2                     11       SPI_SC_SDCARD
 *  PA[02] USART2_TX/ADC12_IN2/TIM2_CH3                      12       SPI_SC_DISPLAY
 *  PA[03] USART2_RX/ADC12_IN3/TIM2_CH4                      13       SPI_D/C
 *  PA[04] SPI1_NSS/USART2_CK/ADC12_IN4                      14       SPI_RES
 *  PA[05] SPI1_SCK/ADC12_IN5                                15		  N.C.
 *  PA[06] SPI1_MISO/ADC12_IN6/TIM3_CH1                      16		  N.C.
 *  PA[07] SPI1_MOSI/ADC12_IN7/TIM3_CH2                      17		  N.C.
 *  PA[08] USART1_CK/TIM1_CH1/MCO                            29		  N.C.
 *  PA[09] USART1_TX/TIM1_CH2                                30       TXD
 *  PA[10] USART1_RX/TIM1_CH3                                31       RXD
 *  PA[11] USART1_CTS/CAN_RX/TIM1_CH4/USBDM                  32       USBM
 *  PA[12] USART1_RTS/CAN_TX/TIM1_ETR/USBDP                  33       USBP
 *  PA[13] JTMS/SWDIO                                        34       JTAG_TMS
 *  PA[14] JTCK/SWCLK                                        37       JTAG_TCK
 *  PA[15] JTDI                                              38       JTAG_TDI
 *
 *  PB[00] ADC12_IN8/TIM3_CH3                                18       BT_LINK
 *  PB[01] ADC12_IN9/TIM3_CH4                                19       N.C.
 *  PB[02] BOOT1                                             20       GND
 *  PB[03] JTDO                                              39       JTAG_TDO
 *  PB[04] NJTRST                                            40       JTAG_nTRST
 *  PB[05] I2C1_SMBA                                         41       N.C.
 *  PB[06] I2C1_SCL/TIM4_CH1                                 42       N.C.
 *  PB[07] I2C1_SDA/TIM4_CH2                                 43       F_T
 *  PB[08] TIM4_CH3                                          45       N.C.
 *  PB[09] TIM4_CH4                                          46       N.C.
 *  PB[10] I2C2_SCL/USART3_TX                                21       N.C.
 *  PB[11] I2C2_SDA/USART3_RX                                22       BT_RESET
 *  PB[12] SPI2_NSS/I2C2_SMBA/USART3_CK/TIM1_BKIN            25       N.C.
 *  PB[13] SPI2_SCK/USART3_CTS/TIM1_CH1N                     26       SPI_CLK
 *  PB[14] SPI2_MISO/USART3_RTS/TIM1_CH2N                    27       SPI_MISO
 *  PB[15] SPI2_MOSI/TIM1_CH3N                               28       SPI_MOSI
 *
 *  PC[13] TAMPER-RTC                                        2        N.C.
 *  PC[14] OSC32_IN                                          3        F_OP
 *  PC[15] OSC32_OUT                                         4        N.C.
 *
 *  PD[00] OSC_IN                                            5        32.768 Hz OSC
 *  PD[01] OSC_OUT                                           6        32.768 Hz OSC
 */

#if STM32_NSPI < 1
#  undef CONFIG_STM32_SPI1
#  undef CONFIG_STM32_SPI2
#elif STM32_NSPI < 2
#  undef CONFIG_STM32_SPI2
#endif

#ifndef CONFIG_R4_2_FLASH_SPI
#define CONFIG_R4_2_FLASH_SPI 2
#endif

/* Display **************************************************************************/
/*
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *
 *  PA[01] USART2_RTS/ADC12_IN1/TIM2_CH2                     11       SPI_SC_DISPLAY
 *  PA[03] USART2_RX/ADC12_IN3/TIM2_CH4                      13       SPI_D/C
 *  PA[04] SPI1_NSS/USART2_CK/ADC12_IN4                      14       SPI_RES
 */

#define GPIO_SPI_SC_DISPLAY (GPIO_PORTA | GPIO_PIN1 | GPIO_OUTPUT_SET | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_SPI_DC			(GPIO_PORTA | GPIO_PIN3 | GPIO_OUTPUT_SET | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_SPI_RES		(GPIO_PORTA | GPIO_PIN4 | GPIO_OUTPUT_SET | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

/* SD card **************************************************************************/
/*
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *
 *  PA[02] USART2_TX/ADC12_IN2/TIM2_CH3                      12       SPI_SC_SDCARD
 */

#define GPIO_SPI_SC_SDCARD	(GPIO_PORTA | GPIO_PIN2 | GPIO_OUTPUT_SET | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

/* Bluetooth **************************************************************************/
/*
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *
 *  PB[00] ADC12_IN8/TIM3_CH3                                18       BT_LINK
 *  PB[11] I2C2_SDA/USART3_RX                                22       BT_RESET
 */

#define GPIO_BT_LINK		(GPIO_PORTB | GPIO_PIN0 | GPIO_INPUT        | GPIO_CNF_INPULLDWN | GPIO_EXTI)
#define GPIO_BT_RESET		(GPIO_PORTB | GPIO_PIN11 | GPIO_OUTPUT_SET  | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

/* USB D+ Pull-up *********************************************************************/
/*
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *
 *  PB[12] SPI2_NSS/I2C2_SMBAl/USART3_CK/TIM1_BKIN	         25       USB_PULLUP
 */

#define GPIO_USB_PULLUP		(GPIO_PORTB | GPIO_PIN12| GPIO_OUTPUT_CLEAR | GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

/* Input freqs  ***********************************************************************/
/*
 *   GPIO      Function                                     MPU        Core
 *                                                          Pin #      Name
 *  ----- --------------------------------             ------------------------
 *
 *  PA[00] WKUP/USART2_CTS/ADC12_IN0/TIM2_CH1_ETR	         10       F_P
 *  PB[07] I2C1_SDA/TIM4_CH2								 43		  F_T
 */

#define GPIO_F_P		(GPIO_PORTA | GPIO_PIN0 | GPIO_INPUT 		| GPIO_CNF_INFLOAT)
#define GPIO_F_T		(GPIO_PORTB | GPIO_PIN7 | GPIO_INPUT 		| GPIO_CNF_INFLOAT)

/************************************************************************************
 * Public Types
 ************************************************************************************/

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_spiinitialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins.
 *
 ************************************************************************************/

void stm32_spiinitialize(void);

/************************************************************************************
 * Name: stm32_usbinitialize
 *
 * Description:
 *   Called to setup USB-related GPIO pins.
 *
 ************************************************************************************/

void stm32_usbinitialize(void);

/************************************************************************************
 * BT
 *
 ************************************************************************************/
#ifdef CONFIG_R4_2_BLUETOOTH
void r4_2_bt_init(void);
void r4_2_bt_reset(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_SPARK_SRC_SPARK_H */
