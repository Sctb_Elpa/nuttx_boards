/****************************************************************************
 * config/r4_2/src/stm32_oled.c
 *
 *   Copyright (C) 2011, 2013, 2015 Gregory Nutt. All rights reserved.
 *   Author: 	Gregory Nutt <gnutt@nuttx.org>
 *   			Shilo_XyZ_ <shilo.xyz@qip.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <nuttx/arch.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/board.h>
#include <nuttx/spi/spi.h>
#include <nuttx/lcd/lcd.h>
#include <../drivers/lcd/ssd1305.h>

#include "r4_2.h"
#include "stm32.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************/
/* This module is only built if CONFIG_NX_LCDDRIVER is selected.  In this
 * case, it would be an error if SSP1 is not also enabled.
 */

#if !defined(CONFIG_STM32_SPI1) && !defined(CONFIG_STM32_SPI2) && !defined(CONFIG_STM32_SPI3)
#  error "The OLED driver requires CONFIG_STM32_SPIx in the configuration"
#endif

#ifndef CONFIG_R4_2_DISPLAY_SPI
#  error "This logic requires R4_2_DISPLAY_SPI in the configuration"
#endif


#ifndef CONFIG_ER_OLEDM024_1G_SPIMODE
#  define CONFIG_ER_OLEDM024_1G_SPIMODE SPIDEV_MODE0
#endif

/* SPI frequency */

#ifndef CONFIG_ER_OLEDM024_1G_FREQUENCY
#  define CONFIG_ER_OLEDM024_1G_FREQUENCY 3500000
#endif

/* CONFIG_ER_OLEDM024_1G_NINTERFACES determines the number of physical interfaces
 * that will be supported.
 */

#ifndef CONFIG_ER_OLEDM024_1G_NINTERFACES
#  define CONFIG_ER_OLEDM024_1G_NINTERFACES 1
#endif

#ifndef CONFIG_ER_OLEDM024_1G_POWER
#	define CONFIG_ER_OLEDM024_1G_POWER
#endif

#if CONFIG_ER_OLEDM024_1G_NINTERFACES != 1
#  warning "Only a single ER-OLEDM024-1G interface is supported"
#  undef CONFIG_ER_OLEDM024_1G_NINTERFACES
#  define CONFIG_ER_OLEDM024_1G_NINTERFACES 1
#endif

/* Orientation */

#if defined(CONFIG_LCD_PORTRAIT) || defined(CONFIG_LCD_RPORTRAIT)
#  warning "No support for portrait modes"
#  define CONFIG_LCD_LANDSCAPE 1
#  undef CONFIG_LCD_PORTRAIT
#  undef CONFIG_LCD_RLANDSCAPE
#  undef CONFIG_LCD_RPORTRAIT
#endif

/* Verbose debug must also be enabled to use the extra OLED debug */

#ifndef CONFIG_DEBUG
#  undef CONFIG_DEBER_VERBOSE
#endif

#ifndef CONFIG_DEBER_VERBOSE
#  undef CONFIG_DEBER_LCD
#endif

/* Check contrast selection */

#ifndef CONFIG_LCD_MAXCONTRAST
#  define CONFIG_LCD_MAXCONTRAST 255
#endif

#if CONFIG_LCD_MAXCONTRAST <= 0 || CONFIG_LCD_MAXCONTRAST > 255
#  error "CONFIG_LCD_MAXCONTRAST exceeds supported maximum"
#endif

#if CONFIG_LCD_MAXCONTRAST < 255
#  warning "Optimal setting of CONFIG_LCD_MAXCONTRAST is 255"
#endif

/* Check power setting */

#if !defined(CONFIG_LCD_MAXPOWER)
#  define CONFIG_LCD_MAXPOWER 2
#endif

#if CONFIG_LCD_MAXPOWER != 2
#  warning "CONFIG_LCD_MAXPOWER should be 2"
#  undef CONFIG_LCD_MAXPOWER
#  define CONFIG_LCD_MAXPOWER 2
#endif

/* The OLED requires CMD/DATA SPI support */

#ifndef CONFIG_SPI_CMDDATA
#  error "CONFIG_SPI_CMDDATA must be defined in your NuttX configuration"
#endif

/* Color is 1bpp monochrome with leftmost column contained in bits 0  */

#ifdef CONFIG_NX_DISABLE_1BPP
#  warning "1 bit-per-pixel support needed"
#endif

/* Color Properties *******************************************************************/
/* The SSD1305 display controller can handle a resolution of 132x64. The OLED
 * on the base board is 128x64.
 */

#define ER_DEV_XRES     132
#define ER_XOFFSET      2//18

/* Display Resolution */

#define ER_LCD_XRES     128
#define ER_LCD_YRES     64

#if defined(CONFIG_LCD_LANDSCAPE) || defined(CONFIG_LCD_RLANDSCAPE)
#  define ER_XRES      ER_LCD_XRES
#  define ER_YRES      ER_LCD_YRES
#else
#  define ER_XRES      ER_LCD_YRES
#  define ER_YRES      ER_LCD_XRES
#endif

/* Color depth and format */

#define ER_BPP          1
#define ER_COLORFMT     FB_FMT_Y1

/* Bytes per logical row and actual device row */

#define ER_XSTRIDE      (ER_XRES >> 3) /* Pixels arrange "horizontally for user" */
#define ER_YSTRIDE      (ER_YRES >> 3) /* But actual device arrangement is "vertical" */

/* The size of the shadow frame buffer */

#define ER_FBSIZE       (ER_XRES * ER_YSTRIDE)

/* Orientation */

#if defined(CONFIG_LCD_LANDSCAPE)
#  undef  ER_LCD_REVERSEX
#  undef  ER_LCD_REVERSEY
#elif defined(CONFIG_LCD_RLANDSCAPE)
#  define ER_LCD_REVERSEX  1
#  define ER_LCD_REVERSEY  1
#endif

/* Bit helpers */

#define LS_BIT          (1 << 0)
#define MS_BIT          (1 << 7)

/* Debug ********************************************************************/
/* Define the CONFIG_DEBER_LCD to enable detailed debug output (stuff you
 * would never want to see unless you are debugging this file).
 *
 * Verbose debug must also be enabled
 */

#ifdef CONFIG_DEBER_LCD
# define lcddbg(format, ...)  vdbg(format, ##__VA_ARGS__)
#else
# define lcddbg(x...)
#endif

/* Some important "colors" */

#define ER_Y1_BLACK  0
#define ER_Y1_WHITE  1

/* Only three power settings are supported: */

#define ER_POWER_OFF 0
#define ER_POWER_DIM 1
#define ER_POWER_ON  2

/**************************************************************************************
 * Private Type Definition
 **************************************************************************************/

/* This structure describes the state of this driver */

struct er_dev_s
{
	/* Publically visible device structure */

	struct lcd_dev_s dev;

	/* Private LCD-specific information follows */

	FAR struct spi_dev_s *spi;
	uint8_t contrast;
	uint8_t powered;

	/* The SSD1305 does not support reading from the display memory in SPI mode.
	 * Since there is 1 BPP and access is byte-by-byte, it is necessary to keep
	 * a shadow copy of the framebuffer memory.
	 */

	uint8_t fb[ER_FBSIZE];
};


/**************************************************************************************
 * Private Function Protototypes
 **************************************************************************************/

/* SPI helpers */

#ifdef CONFIG_SPI_OWNBUS
static inline void er_select(FAR struct spi_dev_s *spi);
static inline void er_deselect(FAR struct spi_dev_s *spi);
#else
static void er_select(FAR struct spi_dev_s *spi);
static void er_deselect(FAR struct spi_dev_s *spi);
#endif

#ifdef CONFIG_ER_OLEDM024_1G_POWER
void er_power(unsigned int devno, bool on);
#endif

/* LCD Data Transfer Methods */

static int er_putrun(fb_coord_t row, fb_coord_t col, FAR const uint8_t *buffer,
		size_t npixels);
static int er_getrun(fb_coord_t row, fb_coord_t col, FAR uint8_t *buffer,
		size_t npixels);

/* LCD Configuration */

static int er_getvideoinfo(FAR struct lcd_dev_s *dev,
		FAR struct fb_videoinfo_s *vinfo);
static int er_getplaneinfo(FAR struct lcd_dev_s *dev, unsigned int planeno,
		FAR struct lcd_planeinfo_s *pinfo);

/* LCD RGB Mapping */

#ifdef CONFIG_FB_CMAP
#  error "RGB color mapping not supported by this driver"
#endif

/* Cursor Controls */

#ifdef CONFIG_FB_HWCURSOR
#  error "Cursor control not supported by this driver"
#endif

/* LCD Specific Controls */

static int er_getpower(struct lcd_dev_s *dev);
static int er_setpower(struct lcd_dev_s *dev, int power);
static int er_getcontrast(struct lcd_dev_s *dev);
static int er_setcontrast(struct lcd_dev_s *dev, unsigned int contrast);

/* Initialization */

static inline void up_clear(FAR struct er_dev_s  *priv);

/************************************************************************************
 * Private Data
 ************************************************************************************/

/* This is working memory allocated by the LCD driver for each LCD device
 * and for each color plane.  This memory will hold one raster line of data.
 * The size of the allocated run buffer must therefore be at least
 * (bpp * xres / 8).  Actual alignment of the buffer must conform to the
 * bitwidth of the underlying pixel type.
 *
 * If there are multiple planes, they may share the same working buffer
 * because different planes will not be operate on concurrently.  However,
 * if there are multiple LCD devices, they must each have unique run buffers.
 */

static uint8_t g_runbuffer[ER_XSTRIDE+1];

/* This structure describes the overall LCD video controller */

static const struct fb_videoinfo_s g_videoinfo =
{
		.fmt     = ER_COLORFMT,    /* Color format: RGB16-565: RRRR RGGG GGGB BBBB */
		.xres    = ER_XRES,        /* Horizontal resolution in pixel columns */
		.yres    = ER_YRES,        /* Vertical resolution in pixel rows */
		.nplanes = 1,              /* Number of color planes supported */
};

/* This is the standard, NuttX Plane information object */

static const struct lcd_planeinfo_s g_planeinfo =
{
		.putrun = er_putrun,             /* Put a run into LCD memory */
		.getrun = er_getrun,             /* Get a run from LCD memory */
		.buffer = (uint8_t*)g_runbuffer, /* Run scratch buffer */
		.bpp    = ER_BPP,                /* Bits-per-pixel */
};

/* This is the standard, NuttX LCD driver object */

static struct er_dev_s g_erdev =
{
		.dev =
		{
				/* LCD Configuration */

				.getvideoinfo = er_getvideoinfo,
				.getplaneinfo = er_getplaneinfo,

				/* LCD RGB Mapping -- Not supported */
				/* Cursor Controls -- Not supported */

				/* LCD Specific Controls */

				.getpower     = er_getpower,
				.setpower     = er_setpower,
				.getcontrast  = er_getcontrast,
				.setcontrast  = er_setcontrast,
		},
};

static struct lcd_dev_s *gp_erdev = NULL;
static uint8_t BreakDraw = false;

/**************************************************************************************
 * Private Functions
 **************************************************************************************/

/**************************************************************************************
 * Name:  er_powerstring
 *
 * Description:
 *   Convert the power setting to a string.
 *
 **************************************************************************************/

static inline FAR const char *er_powerstring(uint8_t power)
{
	if (power == ER_POWER_OFF)
	{
		return "OFF";
	}
	else if (power == ER_POWER_DIM)
	{
		return "DIM";
	}
	else if (power == ER_POWER_ON)
	{
		return "ON";
	}
	else
	{
		return "ERROR";
	}
}

/**************************************************************************************
 * Function: er_select
 *
 * Description:
 *   Select the SPI, locking and  re-configuring if necessary
 *
 * Parameters:
 *   spi  - Reference to the SPI driver structure
 *
 * Returned Value:
 *   None
 *
 * Assumptions:
 *
 **************************************************************************************/

#ifdef CONFIG_SPI_OWNBUS
static inline void er_select(FAR struct spi_dev_s *spi)
{
	/* We own the SPI bus, so just select the chip */

	SPI_SELECT(spi, SPIDEV_DISPLAY, true);
}
#else
static void er_select(FAR struct spi_dev_s *spi)
{
	/* Select UG-9664HSWAG01 chip (locking the SPI bus in case there are multiple
	 * devices competing for the SPI bus
	 */

	SPI_LOCK(spi, true);
	SPI_SELECT(spi, SPIDEV_DISPLAY, true);

	/* Now make sure that the SPI bus is configured for the UG-9664HSWAG01 (it
	 * might have gotten configured for a different device while unlocked)
	 */

	SPI_SETMODE(spi, CONFIG_ER_OLEDM024_1G_SPIMODE);
	SPI_SETBITS(spi, 8);
#ifdef CONFIG_ER_OLEDM024_1G_FREQUENCY
	SPI_SETFREQUENCY(spi, CONFIG_ER_OLEDM024_1G_FREQUENCY);
#endif
}
#endif

/**************************************************************************************
 * Function: er_deselect
 *
 * Description:
 *   De-select the SPI
 *
 * Parameters:
 *   spi  - Reference to the SPI driver structure
 *
 * Returned Value:
 *   None
 *
 * Assumptions:
 *
 **************************************************************************************/

#ifdef CONFIG_SPI_OWNBUS
static inline void er_deselect(FAR struct spi_dev_s *spi)
{
	/* We own the SPI bus, so just de-select the chip */

	SPI_SELECT(spi, SPIDEV_DISPLAY, false);
}
#else
static void er_deselect(FAR struct spi_dev_s *spi)
{
	/* De-select UG-9664HSWAG01 chip and relinquish the SPI bus. */

	SPI_SELECT(spi, SPIDEV_DISPLAY, false);
	SPI_LOCK(spi, false);
}
#endif


/**************************************************************************************
 * Name:  er_putrun
 *
 * Description:
 *   This method can be used to write a partial raster line to the LCD:
 *
 *   row     - Starting row to write to (range: 0 <= row < yres)
 *   col     - Starting column to write to (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer containing the run to be written to the LCD
 *   npixels - The number of pixels to write to the LCD
 *             (range: 0 < npixels <= xres-col)
 *
 **************************************************************************************/

static int er_putrun(fb_coord_t row, fb_coord_t col, FAR const uint8_t *buffer,
		size_t npixels)
{
	/* Because of this line of code, we will only be able to support a single UG device */

	FAR struct er_dev_s *priv = &g_erdev;
	FAR uint8_t *fbptr;
	FAR uint8_t *ptr;
	uint8_t devcol;
	uint8_t fbmask;
	uint8_t page;
	uint8_t usrmask;
	uint8_t i;
	int     pixlen;

	gvdbg("row: %d col: %d npixels: %d\n", row, col, npixels);
	DEBUGASSERT(buffer);

	/* Clip the run to the display */

	pixlen = npixels;
	if ((unsigned int)col + (unsigned int)pixlen > (unsigned int)ER_XRES)
	{
		pixlen = (int)ER_XRES - (int)col;
	}

	/* Verify that some portion of the run remains on the display */

	if (pixlen <= 0 || row > ER_YRES)
	{
		return OK;
	}

	/* Perform coordinate conversion for reverse landscape mode.
	 * If the rows are reversed then rows are are a mirror reflection of
	 * top to bottom.
	 */

#ifdef ER_LCD_REVERSEY
	row = (ER_YRES-1) - row;
#endif

	/* If the column is switched then the start of the run is the mirror of
	 * the end of the run.
	 *
	 *            col+pixlen-1
	 *     col    |
	 *  0  |      |                    XRES
	 *  .  S>>>>>>E                    .
	 *  .                    E<<<<<<S  .
	 *                       |      |
	 *                       |      `-(XRES-1)-col
	 *                       ` (XRES-1)-col-(pixlen-1)
	 */

#ifdef ER_LCD_REVERSEX
	col  = (ER_XRES-1) - col;
	col -= (pixlen - 1);
#endif

	/* Get the page number.  The range of 64 lines is divided up into eight
	 * pages of 8 lines each.
	 */

	page = row >> 3;

	/* Update the shadow frame buffer memory. First determine the pixel
	 * position in the frame buffer memory.  Pixels are organized like
	 * this:
	 *
	 *  --------+---+---+---+---+-...-+-----+
	 *  Segment | 0 | 1 | 2 | 3 | ... | 131 |
	 *  --------+---+---+---+---+-...-+-----+
	 *  Bit 0   |   | X |   |   |     |     |
	 *  Bit 1   |   | X |   |   |     |     |
	 *  Bit 2   |   | X |   |   |     |     |
	 *  Bit 3   |   | X |   |   |     |     |
	 *  Bit 4   |   | X |   |   |     |     |
	 *  Bit 5   |   | X |   |   |     |     |
	 *  Bit 6   |   | X |   |   |     |     |
	 *  Bit 7   |   | X |   |   |     |     |
	 *  --------+---+---+---+---+-...-+-----+
	 *
	 * So, in order to draw a white, horizontal line, at row 45. we
	 * would have to modify all of the bytes in page 45/8 = 5.  We
	 * would have to set bit 45%8 = 5 in every byte in the page.
	 */

	fbmask  = 1 << (row & 7);
	fbptr   = &priv->fb[page * ER_XRES + col];
#ifdef ER_LCD_REVERSEX
	ptr     = fbptr + (pixlen - 1);
#else
	ptr     = fbptr;
#endif

#ifdef CONFIG_NX_PACKEDMSFIRST
	usrmask = MS_BIT;
#else
	usrmask = LS_BIT;
#endif

	for (i = 0; i < pixlen; i++)
	{
		/* Set or clear the corresponding bit */

#ifdef ER_LCD_REVERSEX
		if ((*buffer & usrmask) != 0)
		{
			*ptr-- |= fbmask;
		}
		else
		{
			*ptr-- &= ~fbmask;
		}
#else
		if ((*buffer & usrmask) != 0)
		{
			*ptr++ |= fbmask;
		}
		else
		{
			*ptr++ &= ~fbmask;
		}
#endif

		/* Inc/Decrement to the next source pixel */

#ifdef CONFIG_NX_PACKEDMSFIRST
		if (usrmask == LS_BIT)
		{
			buffer++;
			usrmask = MS_BIT;
		}
		else
		{
			usrmask >>= 1;
		}
#else
		if (usrmask == MS_BIT)
		{
			buffer++;
			usrmask = LS_BIT;
		}
		else
		{
			usrmask <<= 1;
		}
#endif
	}

	/* Offset the column position to account for smaller horizontal
	 * display range.
	 */

	devcol = col + ER_XOFFSET;

	if (!BreakDraw)
	{
		/* Select and lock the device */
		er_select(priv->spi);

		/* Select command transfer */

		SPI_CMDDATA(priv->spi, SPIDEV_DISPLAY, true);

		/* Set the starting position for the run */

		(void)SPI_SEND(priv->spi, SSD1305_SETPAGESTART + page);       /* Set the page start */
		(void)SPI_SEND(priv->spi, SSD1305_SETCOLL + (devcol & 0x0f)); /* Set the low column */
		(void)SPI_SEND(priv->spi, SSD1305_SETCOLH + (devcol >> 4));   /* Set the high column */

		/* Select data transfer */

		SPI_CMDDATA(priv->spi, SPIDEV_DISPLAY, false);

		/* Then transfer all of the data */

		(void)SPI_SNDBLOCK(priv->spi, fbptr, pixlen);

		/* Unlock and de-select the device */

		er_deselect(priv->spi);
	}
	return OK;
}

/**************************************************************************************
 * Name:  er_getrun
 *
 * Description:
 *   This method can be used to read a partial raster line from the LCD.
 *
 *  row     - Starting row to read from (range: 0 <= row < yres)
 *  col     - Starting column to read read (range: 0 <= col <= xres-npixels)
 *  buffer  - The buffer in which to return the run read from the LCD
 *  npixels - The number of pixels to read from the LCD
 *            (range: 0 < npixels <= xres-col)
 *
 **************************************************************************************/

static int er_getrun(fb_coord_t row, fb_coord_t col, FAR uint8_t *buffer,
		size_t npixels)
{
	/* Because of this line of code, we will only be able to support a single UG device */

	FAR struct er_dev_s *priv = &g_erdev;
	FAR uint8_t *fbptr;
	uint8_t page;
	uint8_t fbmask;
	uint8_t usrmask;
	uint8_t i;
	int     pixlen;

	gvdbg("row: %d col: %d npixels: %d\n", row, col, npixels);
	DEBUGASSERT(buffer);

	/* Clip the run to the display */

	pixlen = npixels;
	if ((unsigned int)col + (unsigned int)pixlen > (unsigned int)ER_XRES)
	{
		pixlen = (int)ER_XRES - (int)col;
	}

	/* Verify that some portion of the run is actually the display */

	if (pixlen <= 0 || row > ER_YRES)
	{
		return -EINVAL;
	}

	/* Perform coordinate conversion for reverse landscape mode.
	 * If the rows are reversed then rows are are a mirror reflection of
	 * top to bottom.
	 */

#ifdef ER_LCD_REVERSEY
	row = (ER_YRES-1) - row;
#endif

	/* If the column is switched then the start of the run is the mirror of
	 * the end of the run.
	 *
	 *            col+pixlen-1
	 *     col    |
	 *  0  |      |                    XRES
	 *  .  S>>>>>>E                    .
	 *  .                    E<<<<<<S  .
	 *                       |      |
	 *                       |      `-(XRES-1)-col
	 *                       ` (XRES-1)-col-(pixlen-1)
	 */

#ifdef ER_LCD_REVERSEX
	col  = (ER_XRES-1) - col;
#endif

	/* Then transfer the display data from the shadow frame buffer memory */
	/* Get the page number.  The range of 64 lines is divided up into eight
	 * pages of 8 lines each.
	 */

	page = row >> 3;

	/* Update the shadow frame buffer memory. First determine the pixel
	 * position in the frame buffer memory.  Pixels are organized like
	 * this:
	 *
	 *  --------+---+---+---+---+-...-+-----+
	 *  Segment | 0 | 1 | 2 | 3 | ... | 131 |
	 *  --------+---+---+---+---+-...-+-----+
	 *  Bit 0   |   | X |   |   |     |     |
	 *  Bit 1   |   | X |   |   |     |     |
	 *  Bit 2   |   | X |   |   |     |     |
	 *  Bit 3   |   | X |   |   |     |     |
	 *  Bit 4   |   | X |   |   |     |     |
	 *  Bit 5   |   | X |   |   |     |     |
	 *  Bit 6   |   | X |   |   |     |     |
	 *  Bit 7   |   | X |   |   |     |     |
	 *  --------+---+---+---+---+-...-+-----+
	 *
	 * So, in order to draw a white, horizontal line, at row 45. we
	 * would have to modify all of the bytes in page 45/8 = 5.  We
	 * would have to set bit 45%8 = 5 in every byte in the page.
	 */

	fbmask  = 1 << (row & 7);
	fbptr   = &priv->fb[page * ER_XRES + col];

#ifdef CONFIG_NX_PACKEDMSFIRST
	usrmask = MS_BIT;
#else
	usrmask = LS_BIT;
#endif

	*buffer = 0;
	for (i = 0; i < pixlen; i++)
	{
		/* Set or clear the corresponding bit */

#ifdef ER_LCD_REVERSEX
		uint8_t byte = *fbptr--;
#else
		uint8_t byte = *fbptr++;
#endif

		if ((byte & fbmask) != 0)
		{
			*buffer |= usrmask;
		}

		/* Inc/Decrement to the next destination pixel. Hmmmm. It looks like
		 * this logic could write past the end of the user buffer.  Revisit
		 * this!
		 */

#ifdef CONFIG_NX_PACKEDMSFIRST
		if (usrmask == LS_BIT)
		{
			buffer++;
			*buffer = 0;
			usrmask = MS_BIT;
		}
		else
		{
			usrmask >>= 1;
		}
#else
		if (usrmask == MS_BIT)
		{
			buffer++;
			*buffer = 0;
			usrmask = LS_BIT;
		}
		else
		{
			usrmask <<= 1;
		}
#endif
	}

	return OK;
}


/**************************************************************************************
 * Name:  er_getvideoinfo
 *
 * Description:
 *   Get information about the LCD video controller configuration.
 *
 **************************************************************************************/

static int er_getvideoinfo(FAR struct lcd_dev_s *dev,
		FAR struct fb_videoinfo_s *vinfo)
{
	DEBUGASSERT(dev && vinfo);
	gvdbg("fmt: %d xres: %d yres: %d nplanes: %d\n",
			g_videoinfo.fmt, g_videoinfo.xres, g_videoinfo.yres, g_videoinfo.nplanes);
	memcpy(vinfo, &g_videoinfo, sizeof(struct fb_videoinfo_s));
	return OK;
}

/**************************************************************************************
 * Name:  er_getplaneinfo
 *
 * Description:
 *   Get information about the configuration of each LCD color plane.
 *
 **************************************************************************************/

static int er_getplaneinfo(FAR struct lcd_dev_s *dev, unsigned int planeno,
		FAR struct lcd_planeinfo_s *pinfo)
{
	DEBUGASSERT(dev && pinfo && planeno == 0);
	gvdbg("planeno: %d bpp: %d\n", planeno, g_planeinfo.bpp);
	memcpy(pinfo, &g_planeinfo, sizeof(struct lcd_planeinfo_s));
	return OK;
}

/**************************************************************************************
 * Name:  er_getpower
 *
 * Description:
 *   Get the LCD panel power status (0: full off - CONFIG_LCD_MAXPOWER: full on). On
 *   backlit LCDs, this setting may correspond to the backlight setting.
 *
 **************************************************************************************/

static int er_getpower(struct lcd_dev_s *dev)
{
	struct er_dev_s *priv = (struct er_dev_s *)dev;
	DEBUGASSERT(priv);
	gvdbg("powered: %s\n", er_powerstring(priv->powered));
	return priv->powered;
}

/**************************************************************************************
 * Name:  er_setpower
 *
 * Description:
 *   Enable/disable LCD panel power (0: full off - CONFIG_LCD_MAXPOWER: full on). On
 *   backlit LCDs, this setting may correspond to the backlight setting.
 *
 **************************************************************************************/

static int er_setpower(struct lcd_dev_s *dev, int power)
{
	struct er_dev_s *priv = (struct er_dev_s *)dev;

	DEBUGASSERT(priv && (unsigned)power <= CONFIG_LCD_MAXPOWER);
	gvdbg("power: %s powered: %s\n",
			er_powerstring(power), er_powerstring(priv->powered));

	/* Select and lock the device */

	er_select(priv->spi);
	if (power <= ER_POWER_OFF)
	{
		/* Turn the display off */

		(void)SPI_SEND(priv->spi, SSD1305_DISPOFF);       /* Display off */

		/* Remove power to the device */

		er_power(0, false);
		priv->powered = ER_POWER_OFF;
	}
	else
	{
		/* Turn the display on, dim or normal */

		if (power == ER_POWER_DIM)
		{
			(void)SPI_SEND(priv->spi, SSD1305_DISPONDIM); /* Display on, dim mode */
		}
		else /* if (power > ER_POWER_DIM) */
		{
			(void)SPI_SEND(priv->spi, SSD1305_DISPON);    /* Display on, normal mode */
			power = ER_POWER_ON;
		}
		(void)SPI_SEND(priv->spi, SSD1305_DISPRAM);       /* Resume to RAM content display */

		/* Restore power to the device */

		er_power(0, true);
		priv->powered = power;
	}
	er_deselect(priv->spi);

	return OK;
}

/**************************************************************************************
 * Name:  er_getcontrast
 *
 * Description:
 *   Get the current contrast setting (0-CONFIG_LCD_MAXCONTRAST).
 *
 **************************************************************************************/

static int er_getcontrast(struct lcd_dev_s *dev)
{
	struct er_dev_s *priv = (struct er_dev_s *)dev;
	DEBUGASSERT(priv);
	return (int)priv->contrast;
}

/**************************************************************************************
 * Name:  er_setcontrast
 *
 * Description:
 *   Set LCD panel contrast (0-CONFIG_LCD_MAXCONTRAST).
 *
 **************************************************************************************/

static int er_setcontrast(struct lcd_dev_s *dev, unsigned int contrast)
{
	struct er_dev_s *priv = (struct er_dev_s *)dev;

	gvdbg("contrast: %d\n", contrast);
	DEBUGASSERT(priv);

	if (contrast > 255)
	{
		return -EINVAL;
	}

	/* Select and lock the device */

	er_select(priv->spi);

	/* Select command transfer */

	SPI_CMDDATA(priv->spi, SPIDEV_DISPLAY, true);

	/* Set the contrast */

	(void)SPI_SEND(priv->spi, SSD1305_SETCONTRAST);  /* Set contrast control register */
	(void)SPI_SEND(priv->spi, contrast);             /* Data 1: Set 1 of 256 contrast steps */
	priv->contrast = contrast;

	/* Unlock and de-select the device */

	er_deselect(priv->spi);
	return OK;
}

/**************************************************************************************
 * Name:  up_clear
 *
 * Description:
 *   Clear the display.
 *
 **************************************************************************************/

static inline void up_clear(FAR struct er_dev_s  *priv)
{
	FAR struct spi_dev_s *spi  = priv->spi;
	int page;

	/* Clear the framebuffer */
	memset(priv->fb, ER_Y1_BLACK, ER_FBSIZE);

	/* Select and lock the device */

	er_select(priv->spi);

	/* Go through all 8 pages */

	for (page = 0; page < ER_LCD_YRES / 8; page++)
	{
		/* Select command transfer */

		SPI_CMDDATA(spi, SPIDEV_DISPLAY, true);

		/* Set the starting position for the run */

		(void)SPI_SEND(priv->spi, SSD1305_SETPAGESTART + page);
		(void)SPI_SEND(priv->spi, SSD1305_SETCOLL + (ER_XOFFSET & 0x0f));
		(void)SPI_SEND(priv->spi, SSD1305_SETCOLH + (ER_XOFFSET >> 4));

		/* Select data transfer */

		SPI_CMDDATA(spi, SPIDEV_DISPLAY, false);

		/* Then transfer all 128 columns of data */

		(void)SPI_SNDBLOCK(priv->spi, &priv->fb[page * ER_XRES], ER_XRES);
	}

	/* Unlock and de-select the device */

	er_deselect(spi);
}

/**************************************************************************************
 * Name:  er_initialize
 *
 * Description:
 *   REF: http://www.buydisplay.com/download/democode/ER-OLEDM024-1_Series_8080-8-bit_8080-4-bit_4-wire-spi_DemoCode.txt
 *   Initialize the ER-OLEDM024-1G video hardware.  The initial state of the
 *   OLED is fully initialized, display memory cleared, and the OLED ready to
 *   use, but with the power setting at 0 (full off == sleep mode).
 *
 * Input Parameters:
 *
 *   spi - A reference to the SPI driver instance.
 *   devno - A value in the range of 0 through CONFIG_ER_OLEDM024_1G_NINTERFACES-1.
 *     This allows support for multiple OLED devices.
 *
 * Returned Value:
 *
 *   On success, this function returns a reference to the LCD object for the specified
 *   OLED.  NULL is returned on any failure.
 *
 **************************************************************************************/

static FAR struct lcd_dev_s *er_initialize(FAR struct spi_dev_s *spi, unsigned int devno)
{
	/* Configure and enable LCD */

	FAR struct er_dev_s  *priv = &g_erdev;

	gvdbg("Initializing\n");
	DEBUGASSERT(spi && devno == 0);

	/* Save the reference to the SPI device */

	priv->spi = spi;

	/* Select and lock the device */

	er_select(spi);

	/* Make sure that the OLED off */

	er_power(0, false);

	/* Select command transfer */

	SPI_CMDDATA(spi, SPIDEV_DISPLAY, true);

	/* Configure the device */
#if 0
	(void)SPI_SEND(spi, SSD1305_DISPOFF);/* turn off oled panel */
	(void)SPI_SEND(spi, SSD1305_SETDCLK);/* set display clock divide ratio/oscillator frequency */
	(void)SPI_SEND(spi, 15 << SSD1305_DCLKFREQ_SHIFT | 0 << SSD1305_DCLKDIV_SHIFT);/* set divide ratio */
	(void)SPI_SEND(spi, SSD1305_SETMUX);/* set multiplex ratio(1 to 64) */
	(void)SPI_SEND(spi, 0x3f);/* 1/64 duty */
	(void)SPI_SEND(spi, SSD1305_SETOFFSET);//-set display offset */
	(void)SPI_SEND(spi, 0x40);
	(void)SPI_SEND(spi, SSD1305_MSTRCONFIG);/* Set Master Configuration */
	(void)SPI_SEND(spi, SSD1305_MSTRCONFIG_EXTVCC);
	(void)SPI_SEND(spi, SSD1305_SETCOLORMODE);/* Set Area Color Mode On/Off & Low Power Display Mode */
	(void)SPI_SEND(spi, 0x05);
	(void)SPI_SEND(spi, SSD1305_MAPCOL131);/* set segment re-map 132 to 0 */
	(void)SPI_SEND(spi, SSD1305_SETCOMREMAPPED);/* Set COM Output Scan Direction 64 to 1 */
	(void)SPI_SEND(spi, SSD1305_SETCOMCONFIG);/* Set COM Pins Hardware Configuration */
	(void)SPI_SEND(spi, SSD1305_COMCONFIG_ALT);
	(void)SPI_SEND(spi, SSD1305_SETLUT);/* Set current drive pulse width of BANK0, Color A, Band C. */
	(void)SPI_SEND(spi, 0x3f);
	(void)SPI_SEND(spi, 0x3f);
	(void)SPI_SEND(spi, 0x3f);
	(void)SPI_SEND(spi, 0x3f);
	(void)SPI_SEND(spi, SSD1305_SETCONTRAST);/* set contrast control register */
	(void)SPI_SEND(spi, 0x32);
	(void)SPI_SEND(spi, SSD1305_SETBRIGHTNESS);/* set contrast control register */
	(void)SPI_SEND(spi, 0x80);
	(void)SPI_SEND(spi, SSD1305_SETPRECHARGE);/* set pre-charge period */
	(void)SPI_SEND(spi, 0xf1);
	(void)SPI_SEND(spi, SSD1305_SETVCOMHDESEL);/* set vcomh */
	(void)SPI_SEND(spi, SSD1305_VCOMH_x7p7);
	(void)SPI_SEND(spi, SSD1305_DISPNORMAL);/* set normal display */
	(void)SPI_SEND(spi, SSD1305_DISPRAM);/* Disable Entire Display On */
	(void)SPI_SEND(spi, SSD1305_DISPON);/* turn on oled panel */
#else
	(void)SPI_SEND(spi, SSD1305_SETCOLL + 2);       /* Set low column address */
	(void)SPI_SEND(spi, SSD1305_SETCOLH + 2);       /* Set high column address */
	(void)SPI_SEND(spi, SSD1305_SETSTARTLINE+0);    /* Display start set */
	(void)SPI_SEND(spi, SSD1305_SCROLL_STOP);       /* Stop horizontal scroll */
	(void)SPI_SEND(spi, SSD1305_SETCONTRAST);       /* Set contrast control register */
	(void)SPI_SEND(spi, 0x32);                      /* Data 1: Set 1 of 256 contrast steps */
	(void)SPI_SEND(spi, SSD1305_SETBRIGHTNESS);     /* Brightness for color bank */
	(void)SPI_SEND(spi, 0x80);                      /* Data 1: Set 1 of 256 contrast steps */
	(void)SPI_SEND(spi, SSD1305_MAPCOL131);         /* Set segment re-map */
	(void)SPI_SEND(spi, SSD1305_DISPNORMAL);        /* Set normal display */
	/*(void)SPI_SEND(spi, SSD1305_DISPINVERTED);         Set inverse display */
	(void)SPI_SEND(spi, SSD1305_SETMUX);            /* Set multiplex ratio */
	(void)SPI_SEND(spi, 0x3f);                      /* Data 1: MUX ratio -1: 15-63 */
	(void)SPI_SEND(spi, SSD1305_SETOFFSET);         /* Set display offset */
	(void)SPI_SEND(spi, 0x40);                      /* Data 1: Vertical shift by COM: 0-63 */
	(void)SPI_SEND(spi, SSD1305_MSTRCONFIG);        /* Set dc-dc on/off */
	(void)SPI_SEND(spi, SSD1305_MSTRCONFIG_EXTVCC); /* Data 1: Select external Vcc */
	(void)SPI_SEND(spi, SSD1305_SETCOMREMAPPED);    /* Set com output scan direction */
	(void)SPI_SEND(spi, SSD1305_SETDCLK);           /* Set display clock divide
	 * ratio/oscillator/frequency */
	(void)SPI_SEND(spi, 15 << SSD1305_DCLKFREQ_SHIFT | 0 << SSD1305_DCLKDIV_SHIFT);
	(void)SPI_SEND(spi, SSD1305_SETCOLORMODE);      /* Set area color mode on/off & low power
	 * display mode */
	(void)SPI_SEND(spi, SSD1305_COLORMODE_MONO | SSD1305_POWERMODE_LOW);
	(void)SPI_SEND(spi, SSD1305_SETPRECHARGE);      /* Set pre-charge period */
	(void)SPI_SEND(spi, 15 << SSD1305_PHASE2_SHIFT | 1 << SSD1305_PHASE1_SHIFT);
#ifdef CONFIG_ER_OLEDM024_1G_FLIP
	(void)SPI_SEND(spi, SSD1305_MAPCOL0);			/* 0xa0: Column address 0 is mapped to SEG0 */
	(void)SPI_SEND(spi, SSD1305_SETCOMNORMAL);		/* 0xc0: Set COM output, normal mode */
#else
	(void)SPI_SEND(spi, SSD1305_MAPCOL131);			/* 0xa1: Column address 131 is mapped to SEG0 */
	(void)SPI_SEND(spi, SSD1305_SETCOMREMAPPED);	/* Set COM Output Scan Direction 64 to 1 */
#endif
	(void)SPI_SEND(spi, SSD1305_SETCOMCONFIG);      /* Set COM configuration */
	(void)SPI_SEND(spi, SSD1305_COMCONFIG_ALT);     /* Data 1, Bit 4: 1=Alternative COM pin configuration */
	(void)SPI_SEND(spi, SSD1305_SETVCOMHDESEL);     /* Set VCOMH deselect level */
	(void)SPI_SEND(spi, SSD1305_VCOMH_x7p7);        /* Data 1: ~0.77 x Vcc  */
	(void)SPI_SEND(spi, SSD1305_SETLUT);            /* Set look up table for area color */
	(void)SPI_SEND(spi, 0x3f);                      /* Data 1: Pulse width: 31-63 */
	(void)SPI_SEND(spi, 0x3f);                      /* Data 2: Color A: 31-63 */
	(void)SPI_SEND(spi, 0x3f);                      /* Data 3: Color B: 31-63 */
	(void)SPI_SEND(spi, 0x3f);                      /* Data 4: Color C: 31-63 */
	(void)SPI_SEND(spi, SSD1305_DISPON);            /* Display on, normal mode */
	(void)SPI_SEND(spi, SSD1305_DISPRAM);           /* Resume to RAM content display */
#endif
	/* Let go of the SPI lock and de-select the device */

	er_deselect(spi);

	/* Clear the framebuffer */

	up_mdelay(100);
	up_clear(priv);
	return &priv->dev;
}

/****************************************************************************
 * Name: board_graphics_setup
 *
 * Description:
 *   Called by NX initialization logic to configure the OLED.
 *
 ****************************************************************************/

FAR struct lcd_dev_s *board_graphics_setup(unsigned int devno)
{
	FAR struct spi_dev_s *spi;
	FAR struct lcd_dev_s *dev;

	/* Get the SPI port */

	spi = stm32_spibus_initialize(CONFIG_R4_2_DISPLAY_SPI);
	if (!spi)
	{
		glldbg("Failed to initialize SPI port 1\n");
	}
	else
	{
		/* Bind the SPI port to the OLED */

		dev = er_initialize(spi, devno);
		if (!dev)
		{
			glldbg("Failed to bind SPI port %d to OLED %d: %d\n",
					CONFIG_R4_2_DISPLAY_SPI, devno);
		}
		else
		{
			gllvdbg("Bound SPI port %d to OLED %d\n",
					CONFIG_R4_2_DISPLAY_SPI, devno);

			/* And turn the OLED on (dim) */

			(void)dev->setpower(dev, ER_POWER_DIM);
			return dev;
		}
	}
	return NULL;
}

/****************************************************************************
 * Name:  er_power
 *
 * Description:
 *   If the hardware supports a controllable OLED a power supply, this
 *   interface should be provided.  It may be called by the driver to turn
 *   the OLED power on and off as needed.
 *
 * Input Parameters:
 *
 *   devno - A value in the range of 0 throuh CONFIG_ER_OLEDM024_1G_NINTERFACES-1.
 *     This allows support for multiple OLED devices.
 *   on - true:turn power on, false: turn power off.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

#ifdef CONFIG_ER_OLEDM024_1G_POWER
void er_power(unsigned int devno, bool on)
{
	gllvdbg("power %s\n", on ? "ON" : "OFF");
}
#endif

/************************************************************************************
 * Name:  board_lcd_uninitialize
 *
 * Description:
 *   Unitialize the LCD support
 *
 ************************************************************************************/

void board_lcd_uninitialize(void)
{
	/* Hard reset */
	stm32_gpiowrite(GPIO_SPI_RES, 0);
	up_mdelay(1);
	stm32_gpiowrite(GPIO_SPI_RES, 1);
}

/****************************************************************************
 * Name:  board_lcd_initialize
 *
 * Description:
 *   Initialize the LCD video hardware.  The initial state of the LCD is
 *   fully initialized, display memory cleared, and the LCD ready to use,
 *   but with the power setting at 0 (full off).
 *
 ****************************************************************************/

int board_lcd_initialize(void)
{
	if (gp_erdev)
		return OK;

	stm32_configgpio(GPIO_SPI_DC); 			/* display Data/comand */
	stm32_configgpio(GPIO_SPI_RES); 		/* display chip reset */

	board_lcd_uninitialize();

	gp_erdev = board_graphics_setup(0);

	if (gp_erdev)
		return OK;
	else
		return -ENODEV;
}

/****************************************************************************
 * Name:  board_lcd_getdev
 *
 * Description:
 *   Return a a reference to the LCD object for the specified LCD.  This
 *   allows support for multiple LCD devices.
 *
 ****************************************************************************/

FAR struct lcd_dev_s *board_lcd_getdev(int lcddev)
{
	return gp_erdev;
}

uint8_t board_breakDraw(uint8_t enable_break)
{
	uint8_t res = BreakDraw;
	BreakDraw = enable_break;
	return res;
}

void board_syncfb(void)
{
	FAR struct spi_dev_s *spi  = g_erdev.spi;
	int page;

	/* Select and lock the device */

	er_select(spi);

	/* Go through all 8 pages */

	for (page = 0; page < ER_LCD_YRES / 8; page++)
	{
		/* Select command transfer */

		SPI_CMDDATA(spi, SPIDEV_DISPLAY, true);

		/* Set the starting position for the run */

		(void)SPI_SEND(spi, SSD1305_SETPAGESTART + page);
		(void)SPI_SEND(spi, SSD1305_SETCOLL + (ER_XOFFSET & 0x0f));
		(void)SPI_SEND(spi, SSD1305_SETCOLH + (ER_XOFFSET >> 4));

		/* Select data transfer */

		SPI_CMDDATA(spi, SPIDEV_DISPLAY, false);

		/* Then transfer all 128 columns of data */

		(void)SPI_SNDBLOCK(spi, &g_erdev.fb[page * ER_XRES], ER_XRES);
	}

	/* Unlock and de-select the device */

	er_deselect(spi);
}
