/*
 * stm32_timers.h
 *
 *  Created on: 01 июня 2015 г.
 *      Author: tolyan
 */

#ifndef CONFIGS_R4_2_INCLUDE_STM32_TIMERS_H_
#define CONFIGS_R4_2_INCLUDE_STM32_TIMERS_H_

#define R4_2_MASTER_FREQUENCY	(STM32_APB2_TIM1_CLKIN)
#define FAILURE_DETECTOR_OK		5
#define FAILURE_DETECTOR_NO_SIG	0

struct s16bitCounter {
	uint16_t Register;
	uint16_t Extantion;
} __attribute__((packed));

union un16bitCounter
{
	struct s16bitCounter s;
	uint32_t value;
};

struct mesureResult
{
	uint32_t value;
	uint32_t period;
	uint32_t ready;
	uint32_t newperiod;
	uint32_t Detector_ok;
};

enum enMesureType
{
	MT_PRESSURE,
	MT_TEMPERATURE,
};

int enable_chanel(enum enMesureType chanel, bool enable);
struct mesureResult* getResultPointer(enum enMesureType chanel);

#endif /* CONFIGS_R4_2_INCLUDE_STM32_TIMERS_H_ */
