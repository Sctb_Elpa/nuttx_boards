/****************************************************************************
 * config/spark/src/stm32_nsh.c
 *
 *   Copyright (C) 2012-2013 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           David_s5 <david_s5@nscdg.com>
 *           Shilo_XyZ_ <shilo.xyz@qip.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>

#include <nuttx/board.h>
#include <nuttx/kmalloc.h>

#ifdef CONFIG_MMCSD_SPI
#  include <nuttx/spi/spi.h>
#  include <nuttx/mmcsd.h>
#  include <sys/mount.h>
#endif

#ifdef CONFIG_SYSTEM_USBMONITOR
#  include <apps/usbmonitor.h>
#endif

#ifdef CONFIG_USBDEV
#  include "stm32_usbdev.h"
#endif

#include "stm32.h"
#include "productomer.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/

/* Can't support the MMCSD device if it SPI2 or MMCSD support is not enabled */

#define HAVE_MMCSD  1
#if !defined(CONFIG_STM32_SPI2) || !defined(CONFIG_MMCSD_SPI)
#  undef HAVE_MMCSD
#endif

/* Can't support MMCSD features if mountpoints are disabled */

#if defined(CONFIG_DISABLE_MOUNTPOINT)
#  undef HAVE_MMCSD
#endif

/* default mount point */

#ifndef CONFIG_PRODUCTOMER_FLASH_MOUNT_POINT
#  define CONFIG_PRODUCTOMER_FLASH_MOUNT_POINT "/mnt/p%d"
#endif

/* Use minor device number 0 is not is provided */

#ifndef CONFIG_PRODUCTOMER_FLASH_MINOR
#  define CONFIG_PRODUCTOMER_FLASH_MINOR 0
#endif

#ifndef CONFIG_PRODUCTOMER_FLASH_SLOTNO
#  define CONFIG_PRODUCTOMER_FLASH_SLOTNO 0
#endif

/* Can't support both FAT and NXFFS */

#if defined(CONFIG_FS_FAT) && defined(CONFIG_FS_NXFFS)
#  warning "Can't support both FAT and NXFFS -- using FAT"
#endif

#define HAVE_USBDEV     1
#define HAVE_USBMONITOR 1

/* Can't support USB device is USB device is not enabled */

#ifndef CONFIG_USBDEV
#  undef HAVE_USBDEV
#  undef HAVE_USBMONITOR
#endif

/* Check if we should enable the USB monitor before starting NSH */

#if !defined(CONFIG_USBDEV_TRACE) || !defined(CONFIG_SYSTEM_USBMONITOR)
#  undef HAVE_USBMONITOR
#endif

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

extern uint8_t pher_init;

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_app_initialize
 *
 * Description:
 *   Perform architecture specific initialization
 *
 ****************************************************************************/

int board_app_initialize(void)
{
	if (pher_init)
		return OK;

	pher_init = true;

#ifdef HAVE_MMCSD
	FAR struct spi_dev_s *spi;
	int ret;

	/* Configure SPI-based devices */

	/* Get the SPI port */

	syslog(LOG_INFO, "Initializing SPI port %d\n",
			CONFIG_R4_2_FLASH_SPI);

	spi = up_spiinitialize(CONFIG_R4_2_FLASH_SPI);
	if (!spi)
	{
		syslog(LOG_ERR, "ERROR: Failed to initialize SPI port %d\n",
				CONFIG_R4_2_FLASH_SPI);
		return -ENODEV;
	}

	syslog(LOG_INFO, "Successfully initialized SPI port %d\n",
			CONFIG_R4_2_FLASH_SPI);

	/* Now bind the SPI interface to the SST25 SPI FLASH driver */

	syslog(LOG_INFO, "Binding SPI port %d to MMC/SD slot %d\n",
			CONFIG_R4_2_FLASH_SPI, CONFIG_R4_2_FLASH_SLOTNO);

	ret = mmcsd_spislotinitialize(CONFIG_R4_2_FLASH_MINOR, CONFIG_R4_2_FLASH_SLOTNO, spi);

	if (ret < 0)
	{
		syslog(LOG_ERR, "ERROR: Failed to bind SPI port %d to MMC/SD slot %d: %d\n",
				CONFIG_R4_2_FLASH_SPI, CONFIG_R4_2_FLASH_SLOTNO, ret);
		return ret;
	}

	syslog(LOG_INFO, "Successfuly bound SPI port %d to MMC/SD slot %d\n",
			CONFIG_R4_2_FLASH_SPI, CONFIG_R4_2_FLASH_SLOTNO);

#if CONFIG_R4_2_MOUNT_FLASH
	char  partname[16];
	char  mntpoint[16];

	/* mount -t vfat /dev/mmcsd0 /mnt/p0 */

	snprintf(partname, sizeof(partname), "/dev/mmcsd%d",
			CONFIG_R4_2_FLASH_MINOR);
	snprintf(mntpoint, sizeof(mntpoint)-1, CONFIG_R4_2_FLASH_MOUNT_POINT,
			CONFIG_R4_2_FLASH_MINOR);

	/* Mount the file system at /mnt/pn  */

	ret = mount(partname, mntpoint, "vfat", 0, NULL);
	if (ret < 0)
	{
		fdbg("ERROR: Failed to mount the FAT volume: %d\n", errno);
		return ret;
	}

#endif

#endif /* HAVE_MMCSD */

#ifdef HAVE_USBMONITOR
	/* Start the USB Monitor */

	ret = usbmonitor_start(0, NULL);
	if (ret != OK)
	{
		syslog(LOG_ERR, "ERROR: Failed to start USB monitor: %d\n", ret);
	}
#endif

	return OK;
}


int usbmsc_archinitialize(void)
{
#if defined(CONFIG_NSH_ARCHINIT)
	return OK;
#else
	return board_app_initialize();
#endif
}
